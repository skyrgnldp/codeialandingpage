import React, { Component } from 'react';
import { Switch, Router, Route } from 'react-router-dom';
import history from './js/history';

import BodyIndex from './components/BodyIndex';
import Error404 from './components/Error404';

import './App.css';
import 'bootstrap/dist/css/bootstrap.css';

class App extends Component {
  render() {
    return (
      <div className="App">
        <Router history={history}>
          <Switch>
            <Route exact path="/" component={BodyIndex} />
            <Route component={Error404} />
          </Switch>
        </Router>
      </div>
    );
  }
}

export default App;
